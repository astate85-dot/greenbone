#!/usr/bin/env python3

import time
from gvm.connections import UnixSocketConnection
from gvm.protocols.latest import Gmp

# Unix socket connection
connection = UnixSocketConnection(path='/run/gvmd/gvmd.sock')

# Create GMP instance
with Gmp(connection) as gmp:
    gmp.authenticate('username', 'password')  # Replace with your username and password

    # Create a new target
    target_id = gmp.create_target('Auto Scan Target', hosts=['192.168.1.0/24'])

    # Create a new task (replace config_id with the ID of the scan configuration you want to use)
    task_id = gmp.create_task(name='Auto Scan Task', config_id='daba56c8-73ec-11df-a475-002264764cea', target_id=target_id)

    # Schedule the task to run daily
    gmp.create_schedule(name='Daily Schedule', first_time={'hour': 0, 'minute': 0, 'second': 0, 'year': 2023, 'month': 5, 'day': 26}, duration={'unit': 'hour', 'duration': 1}, period={'unit': 'day', 'duration': 1}, task_id=task_id)

    # Start the task immediately
    gmp.start_task(task_id)
